# Conversion of .dav to .mp4 to rtsp in Docker

## Requirement 
### RTSP server image : aler9/rtsp-simple-server
```
docker pull aler9/rtsp-simple-server
```
### FFMPEG image: jrottenberg/ffmpeg
```
docker pull jrottenberg/ffmpeg
```

## 1. Download jrottenberg/ffmpeg image and convert dav to mp4 
```
docker run -it --rm -v $(pwd):$(pwd) -w $(pwd) jrottenberg/ffmpeg -re -i {path=of-dav} -c:v libx264 -f mp4 {name.mp4}
```
Example code:
```
docker run -it --rm -v $(pwd):$(pwd) -w $(pwd) jrottenberg/ffmpeg -re -i /Users/Downloads/NVR_ch12_main_20220408143525_20220408143800.dav -c:v libx264 -f mp4 test.mp4
```



## 2. Create a txt file that linked to the targeted videos to merge
```
# txt file

file 'path-of-video1'
file 'path-of-video2'
file 'path-of-video3'
```
Example
```
# txt file

file '/Users/Desktop/nvr_mp4/nvr_ch5/nvr_ch5_01.mp4'
file '/Users/Desktop/nvr_mp4/nvr_ch5/nvr_ch5_02.mp4'
file '/Users/Desktop/nvr_mp4/nvr_ch5/nvr_ch5_03.mp4'
```



## 3. Merge mp4 videos
```
docker run --rm -v $(pwd):$(pwd) -w $(pwd) jrottenberg/ffmpeg -safe 0 -f concat -i {path-of-txt-file} -c copy {name.mp4}
```
Example code
```
docker run --rm -v $(pwd):$(pwd) -w $(pwd) jrottenberg/ffmpeg -safe 0 -f concat -i /Users/Desktop/nvr_mp4/nvr_ch13/nvr_ch13.txt -c copy nvr_ch13_merged.mp4
```



## 4. Create new network 
```
docker network create {network-name}
```
Example code:
```
docker network create network001
```



## 5. Download and launch the aler9/rtsp-simple-server image:
```
docker run -d --rm -it --name {name-of-rtsp} --network={network-name} -p 8554:8554 aler9/rtsp-simple-server
```
Example code:
```
docker run -d --rm -it --name rtsp-simple-server --network=network001 -p 8554:8554 aler9/rtsp-simple-server
```



## 6. Publish a stream with ffmpeg
```
docker run -it --rm --name {name} --network=
{network-name} -v $(pwd):$(pwd) -w $(pwd) -d jrottenberg/ffmpeg -re -stream_loop -1 -i {path-of-merged-video} -c copy -f rtsp rtsp://
{name-of-rtsp}:8554/{stream-name}
```
Example code:
```
docker run -it --rm --name nvr_ch12_ffmpeg --network=network001 -v $(pwd):$(pwd) -w $(pwd) -d jrottenberg/ffmpeg -re -stream_loop -1 -i /Users/Desktop/nvr_mp4/nvr_ch13/nvr_ch13_merged.mp4 -c copy -f rtsp rtsp://rtsp-simple-server:8554/teststream
```



## 7. Open the stream with VLC
```
vlc rtsp://localhost:8554/{stream-name}
```
Example code:
```
vlc rtsp://localhost:8554/teststream
```
